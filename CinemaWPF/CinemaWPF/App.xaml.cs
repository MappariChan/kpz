﻿using CinemaWPF.Models;
using CinemaWPF.Services;
using CinemaWPF.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols;
using MobileStationService.Services;
using MobileStationService.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CinemaWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        

        public App()
        {
            InitializeServices();
        }

        public static IServiceProvider Services { get; private set; } = null!;

        private void InitializeServices() 
        {
            var services = new ServiceCollection();
            services.AddDbContext<CinemaContext>();
            services.AddSingleton<IMovieService, MovieService>();
            services.AddSingleton<INavigationService, NavigationService>();

            services.AddSingleton<MainViewModel>();
            services.AddSingleton<ShellViewModel>();
            Services = services.BuildServiceProvider();
        }
    }
}
