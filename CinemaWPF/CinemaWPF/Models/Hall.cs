﻿using System;
using System.Collections.Generic;

namespace CinemaWPF.Models;

public partial class Hall
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public double? Square { get; set; }

    public virtual ICollection<MovieSession> MovieSessions { get; set; } = new List<MovieSession>();

    public virtual ICollection<Seat> Seats { get; set; } = new List<Seat>();
}
