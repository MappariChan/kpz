﻿using System;
using System.Collections.Generic;

namespace CinemaWPF.Models;

public partial class Movie
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public int? MovieGenreid { get; set; }

    public TimeOnly? DurationTime { get; set; }

    public virtual MovieGenre? MovieGenre { get; set; }

    public virtual ICollection<MovieSession> MovieSessions { get; set; } = new List<MovieSession>();
}
