﻿using System;
using System.Collections.Generic;

namespace CinemaWPF.Models;

public partial class MovieGenre
{
    public int Id { get; set; }

    public string? Genre { get; set; }

    public virtual ICollection<Movie> Movies { get; set; } = new List<Movie>();
}
