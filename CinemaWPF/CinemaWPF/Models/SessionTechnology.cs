﻿using System;
using System.Collections.Generic;

namespace CinemaWPF.Models;

public partial class SessionTechnology
{
    public int Id { get; set; }

    public string? Technology { get; set; }

    public virtual ICollection<MovieSession> MovieSessions { get; set; } = new List<MovieSession>();
}
