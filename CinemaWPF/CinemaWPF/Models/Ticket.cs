﻿using System;
using System.Collections.Generic;

namespace CinemaWPF.Models;

public partial class Ticket
{
    public int Id { get; set; }

    public int? ViewerId { get; set; }

    public int? SeatId { get; set; }

    public int? MovieSessionId { get; set; }

    public double? Price { get; set; }

    public virtual MovieSession? MovieSession { get; set; }

    public virtual Seat? Seat { get; set; }

    public virtual Viewer? Viewer { get; set; }
}
