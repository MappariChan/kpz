﻿using CinemaWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaWPF.Services
{
    public interface IMovieService
    {
        Task<int> AddMovieAsync(Movie movie);
        Task DeleteMovieAsync(int id);
        Task<List<Movie>> GetMoviesAsync();
    }
}
