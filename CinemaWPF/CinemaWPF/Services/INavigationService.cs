﻿using CinemaWPF.Models;
using System.Collections.Generic;
using System.Windows.Controls;

namespace MobileStationService.Services
{
    public interface INavigationService
    {
        Frame? RootFrame { get; set; }

        void NavigateToMainView();
        void NavigateToMovieGenresView(IEnumerable<MovieGenre> phoneCalls);
        void NavigateToNewMovieView();
    }
}