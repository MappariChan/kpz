﻿using CinemaWPF.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaWPF.Services
{
    public class MovieService : IMovieService
    {
        private CinemaContext _context;

        public MovieService(CinemaContext context) { 
            _context = context;
        }

        public async Task<int> AddMovieAsync(Movie movie) 
        {
            _context.Set<Movie>().Add(movie);
            await _context.SaveChangesAsync();
            return movie.Id;
        }
        public async Task DeleteMovieAsync(int id) {
            var movie = new Movie()
            {
                Id = id
            };
            var entry = _context.Set<Movie>().Entry(movie);
            entry.State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            await _context.SaveChangesAsync();
        }
        public async Task<List<Movie>> GetMoviesAsync() { 
            var movies = await _context.Set<Movie>().Include(m => m.MovieGenre).ToListAsync();
            return movies;
        }
    }
}
