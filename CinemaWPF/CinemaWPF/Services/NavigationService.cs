﻿using CinemaWPF.Models;
using CinemaWPF.Views;
using System.Collections.Generic;
using System.Windows.Controls;

namespace MobileStationService.Services
{
    public class NavigationService : INavigationService
    {
        public Frame? RootFrame { get; set; }

        public void NavigateToMovieGenresView(IEnumerable<MovieGenre> phoneCalls)
        {
            //var phoneCallsViewParameter = new PhoneCallsViewParameter()
            //{
            //    PhoneCalls = phoneCalls
            //};
            //var phoneCallsView = new PhoneCallsView(phoneCallsViewParameter);
            //RootFrame?.Navigate(phoneCallsView);
        }

        public void NavigateToMainView()
        {
            var mainView = new MainView();
            RootFrame?.Navigate(mainView);
        }

        public void NavigateToNewMovieView()
        {
            //var newConsumerView = new NewConsumerView();
            //RootFrame?.Navigate(newConsumerView);
        }
    }
}
