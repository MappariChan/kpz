﻿using CinemaWPF.Models;
using CinemaWPF.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CinemaWPF.Extensions;
using CommunityToolkit.Mvvm.Input;

namespace CinemaWPF.ViewModels
{
    public partial class MainViewModel : ObservableObject
    {
        private IMovieService _movieService;
        [ObservableProperty]
        private ObservableCollection<Movie> movies = new();

        public MainViewModel(IMovieService movieService) { 
            _movieService = movieService;
            InitializeCommands();
        }

        public async Task InitializeAsync()
        { 
            var movies = await _movieService.GetMoviesAsync();
            Movies = new ObservableCollection<Movie>(movies);
        }

        public ICommand DeleteMovieCommand { get; private set; } = null!;

        private void InitializeCommands()
        {
            DeleteMovieCommand = new AsyncRelayCommand<int>(DeleteMovieAsync);
        }

        private async Task DeleteMovieAsync(int Id)
        {
            Movies.RemoveOne(m => m.Id == Id);
            await _movieService.DeleteMovieAsync(Id);
        }
    }
}
