﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using MobileStationService.Services;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MobileStationService.ViewModels
{
    public class ShellViewModel : ObservableObject
    {
        private readonly INavigationService navigationService;

        public ShellViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            InitializeCommands();
        }

        public ICommand OpenMovieTableCommand { get; private set; } = null!;

        private void InitializeCommands()
        {
            OpenMovieTableCommand = new RelayCommand(navigationService.NavigateToMainView);
        }
    }
}
