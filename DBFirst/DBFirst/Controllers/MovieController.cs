﻿using DBFirst.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DBFirst.Models.DTOs;

namespace DBFirst.Controllers
{
    [Route("api/movies")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private CinemaContext _context;

        public MovieController(CinemaContext context) {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<Movie>>> getMovies() {
            IQueryable<Movie> query = _context.Set<Movie>();

            var movies = await query.Include(m => m.MovieGenre).ToListAsync();

            return Ok(movies);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Movie>> getMovie(int id)
        {
            IQueryable<Movie> query = _context.Set<Movie>();

            var movie = await query.Where(m => m.Id == id).Include(m => m.MovieGenre).FirstOrDefaultAsync();

            return Ok(movie);
        }

        [HttpPost]
        public async Task<ActionResult> addMovie([FromBody] CreateMovieDTO movie)
        {
            Movie model = new Movie();
            model.Name = movie.Name;
            model.DurationTime = movie.DurationTime;
            model.MovieGenreid = movie.MovieGenreid;

            _context.Update(model);
            await _context.SaveChangesAsync();
            
            return Ok();
        }
    }
}
