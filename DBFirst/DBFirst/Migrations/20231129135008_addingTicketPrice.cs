﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DBFirst.Migrations
{
    /// <inheritdoc />
    public partial class addingTicketPrice : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "ticket",
                type: "double precision",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "ticket");
        }
    }
}
