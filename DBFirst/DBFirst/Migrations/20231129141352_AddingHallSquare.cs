﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DBFirst.Migrations
{
    /// <inheritdoc />
    public partial class AddingHallSquare : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Square",
                table: "hall",
                type: "double precision",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Square",
                table: "hall");
        }
    }
}
