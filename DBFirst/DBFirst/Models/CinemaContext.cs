﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DBFirst.Models;

public partial class CinemaContext : DbContext
{
    public CinemaContext()
    {
    }

    public CinemaContext(DbContextOptions<CinemaContext> options)
        : base(options)
    {
    }

    public virtual DbSet<DiscountCard> DiscountCards { get; set; }

    public virtual DbSet<Hall> Halls { get; set; }

    public virtual DbSet<Movie> Movies { get; set; }

    public virtual DbSet<MovieGenre> MovieGenres { get; set; }

    public virtual DbSet<MovieSession> MovieSessions { get; set; }

    public virtual DbSet<Seat> Seats { get; set; }

    public virtual DbSet<SeatType> SeatTypes { get; set; }

    public virtual DbSet<SessionTechnology> SessionTechnologies { get; set; }

    public virtual DbSet<Ticket> Tickets { get; set; }

    public virtual DbSet<Viewer> Viewers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseNpgsql("Host=localhost;Database=cinema;Username=postgres;Password=290320044773");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<DiscountCard>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("discount_card_pkey");

            entity.ToTable("discount_card");

            entity.HasIndex(e => e.ViewerId, "discount_card_viewer_id_key").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Discount).HasColumnName("discount");
            entity.Property(e => e.DiscountTitle)
                .HasColumnType("character varying")
                .HasColumnName("discount_title");
            entity.Property(e => e.ViewerId).HasColumnName("viewer_id");

            entity.HasOne(d => d.Viewer).WithOne(p => p.DiscountCard)
                .HasForeignKey<DiscountCard>(d => d.ViewerId)
                .HasConstraintName("discount_card_viewer_id_fkey");
        });

        modelBuilder.Entity<Hall>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("hall_pkey");

            entity.ToTable("hall");

            entity.HasIndex(e => e.Name, "hall_name_key").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
        });

        modelBuilder.Entity<Movie>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("movie_pkey");

            entity.ToTable("movie");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.DurationTime).HasColumnName("duration_time");
            entity.Property(e => e.MovieGenreid).HasColumnName("movie_genreid");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");

            entity.HasOne(d => d.MovieGenre).WithMany(p => p.Movies)
                .HasForeignKey(d => d.MovieGenreid)
                .HasConstraintName("fk_movie_genreid");
        });

        modelBuilder.Entity<MovieGenre>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("movie_genre_pkey");

            entity.ToTable("movie_genre");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Genre)
                .HasColumnType("character varying")
                .HasColumnName("genre");
        });

        modelBuilder.Entity<MovieSession>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("movie_session_pkey");

            entity.ToTable("movie_session");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Datetime)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("datetime");
            entity.Property(e => e.Hallid).HasColumnName("hallid");
            entity.Property(e => e.Movieid).HasColumnName("movieid");
            entity.Property(e => e.SessionTechnologyid).HasColumnName("session_technologyid");

            entity.HasOne(d => d.Hall).WithMany(p => p.MovieSessions)
                .HasForeignKey(d => d.Hallid)
                .HasConstraintName("movie_session_hallid_fkey");

            entity.HasOne(d => d.Movie).WithMany(p => p.MovieSessions)
                .HasForeignKey(d => d.Movieid)
                .HasConstraintName("movie_session_movieid_fkey");

            entity.HasOne(d => d.SessionTechnology).WithMany(p => p.MovieSessions)
                .HasForeignKey(d => d.SessionTechnologyid)
                .HasConstraintName("fk_session_technologyid");
        });

        modelBuilder.Entity<Seat>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("seat_pkey");

            entity.ToTable("seat");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.HallId).HasColumnName("hall_id");
            entity.Property(e => e.SeatNumber).HasColumnName("seat_number");
            entity.Property(e => e.SeatRow).HasColumnName("seat_row");
            entity.Property(e => e.SeatTypeId).HasColumnName("seat_type_id");

            entity.HasOne(d => d.Hall).WithMany(p => p.Seats)
                .HasForeignKey(d => d.HallId)
                .HasConstraintName("seat_hall_id_fkey");

            entity.HasOne(d => d.SeatType).WithMany(p => p.Seats)
                .HasForeignKey(d => d.SeatTypeId)
                .HasConstraintName("seat_seat_type_id_fkey");
        });

        modelBuilder.Entity<SeatType>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("seat_type_pkey");

            entity.ToTable("seat_type");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.TypeName)
                .HasColumnType("character varying")
                .HasColumnName("type_name");
        });

        modelBuilder.Entity<SessionTechnology>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("session_technology_pkey");

            entity.ToTable("session_technology");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Technology)
                .HasColumnType("character varying")
                .HasColumnName("technology");
        });

        modelBuilder.Entity<Ticket>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("ticket");

            entity.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("id");
            entity.Property(e => e.MovieSessionId).HasColumnName("movie_session_id");
            entity.Property(e => e.SeatId).HasColumnName("seat_id");
            entity.Property(e => e.ViewerId).HasColumnName("viewer_id");

            entity.HasOne(d => d.MovieSession).WithMany()
                .HasForeignKey(d => d.MovieSessionId)
                .HasConstraintName("ticket_movie_session_id_fkey");

            entity.HasOne(d => d.Seat).WithMany()
                .HasForeignKey(d => d.SeatId)
                .HasConstraintName("ticket_seat_id_fkey");

            entity.HasOne(d => d.Viewer).WithMany()
                .HasForeignKey(d => d.ViewerId)
                .HasConstraintName("ticket_viewer_id_fkey");
        });

        modelBuilder.Entity<Viewer>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("viewer_pkey");

            entity.ToTable("viewer");

            entity.HasIndex(e => e.PhoneNumber, "u_phone_number").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
            entity.Property(e => e.PhoneNumber)
                .HasColumnType("character varying")
                .HasColumnName("phone_number");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
