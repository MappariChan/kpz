﻿namespace DBFirst.Models.DTOs
{
    public class CreateMovieDTO
    {
        public string? Name { get; set; }

        public int? MovieGenreid { get; set; }

        public TimeOnly? DurationTime { get; set; }
    }
}
