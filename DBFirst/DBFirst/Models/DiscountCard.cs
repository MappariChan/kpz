﻿using System;
using System.Collections.Generic;

namespace DBFirst.Models;

public partial class DiscountCard
{
    public int Id { get; set; }

    public int? ViewerId { get; set; }

    public string? DiscountTitle { get; set; }

    public double? Discount { get; set; }

    public virtual Viewer? Viewer { get; set; }
}
