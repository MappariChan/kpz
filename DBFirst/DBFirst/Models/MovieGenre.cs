﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DBFirst.Models;

public partial class MovieGenre
{
    public int Id { get; set; }

    public string? Genre { get; set; }

    [JsonIgnore]
    public virtual ICollection<Movie> Movies { get; set; } = new List<Movie>();
}
