﻿using System;
using System.Collections.Generic;

namespace DBFirst.Models;

public partial class MovieSession
{
    public int Id { get; set; }

    public int? Hallid { get; set; }

    public int? Movieid { get; set; }

    public int? SessionTechnologyid { get; set; }

    public DateTime? Datetime { get; set; }

    public virtual Hall? Hall { get; set; }

    public virtual Movie? Movie { get; set; }

    public virtual SessionTechnology? SessionTechnology { get; set; }
}
