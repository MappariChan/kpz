﻿using System;
using System.Collections.Generic;

namespace DBFirst.Models;

public partial class Seat
{
    public int Id { get; set; }

    public int? SeatRow { get; set; }

    public int? SeatNumber { get; set; }

    public int? HallId { get; set; }

    public int? SeatTypeId { get; set; }

    public virtual Hall? Hall { get; set; }

    public virtual SeatType? SeatType { get; set; }
}
