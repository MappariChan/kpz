﻿using System;
using System.Collections.Generic;

namespace DBFirst.Models;

public partial class SeatType
{
    public int Id { get; set; }

    public string? TypeName { get; set; }

    public virtual ICollection<Seat> Seats { get; set; } = new List<Seat>();
}
