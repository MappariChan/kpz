﻿using System;
using System.Collections.Generic;

namespace DBFirst.Models;

public partial class Viewer
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? PhoneNumber { get; set; }

    public virtual DiscountCard? DiscountCard { get; set; }
}
