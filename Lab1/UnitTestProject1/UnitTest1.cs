﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Vantukh.Matvii.RobotChallenge;
using Robot.Common;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIfItGoesToTheNearestStationCluster()
        {
            var algorythm = new VantukhAlgorythm();
            List<EnergyStation> energyStations = new List<EnergyStation>() 
            { 
                new EnergyStation()
                { 
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(10, 10)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(50, 50)
                }
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>() { 
                new Robot.Common.Robot()
                {
                    Energy = 100,
                    Position = new Position(20, 10)
                }
            };
            Map map = new Map() { 
                Stations = energyStations,
            };
            var result = algorythm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(12, 10), ((MoveCommand)result).NewPosition);
        }

        [TestMethod]
        public void TestIfItGoesToTheNearestBigestStationCluster()
        {
            var algorythm = new VantukhAlgorythm();
            List<EnergyStation> energyStations = new List<EnergyStation>()
            {
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(10, 11)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(10, 10)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(20, 20)
                }
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot()
                {
                    Energy = 100,
                    Position = new Position(20, 10)
                }
            };
            Map map = new Map()
            {
                Stations = energyStations,
            };
            var result = algorythm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(12, 10), ((MoveCommand)result).NewPosition);
        }

        [TestMethod]
        public void TestIfItCollectsEnergy()
        {
            var algorythm = new VantukhAlgorythm();
            List<EnergyStation> energyStations = new List<EnergyStation>()
            {
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(10, 11)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(10, 10)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(20, 20)
                }
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot()
                {
                    Energy = 100,
                    Position = new Position(20, 10),
                    OwnerName = "Vantukh Matvii",
                }
            };
            Map map = new Map()
            {
                Stations = energyStations,
            };
            var step1 = algorythm.DoStep(robots, 0, map);
            robots[0].Position = ((MoveCommand)step1).NewPosition;
            var step2 = algorythm.DoStep(robots, 0, map);
            Assert.IsInstanceOfType(step2, typeof(CollectEnergyCommand));
        }

        [TestMethod]
        public void TestIfItCreatesNewRobot()
        {
            var algorythm = new VantukhAlgorythm();
            List<EnergyStation> energyStations = new List<EnergyStation>()
            {
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(10, 11)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(10, 10)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(20, 20)
                }
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot()
                {
                    Energy = 1000,
                    Position = new Position(20, 10),
                    OwnerName = "Vantukh Matvii",
                }
            };
            Map map = new Map()
            {
                Stations = energyStations,
            };
            var step1 = algorythm.DoStep(robots, 0, map);
            robots[0].Position = ((MoveCommand)step1).NewPosition;
            var step2 = algorythm.DoStep(robots, 0, map); 
            Assert.IsInstanceOfType(step2, typeof(CreateNewRobotCommand));
        }

        [TestMethod]
        public void TestIfRobotGoesToTheNewStationCluster()
        {
            var algorythm = new VantukhAlgorythm();
            List<EnergyStation> energyStations = new List<EnergyStation>()
            {
                new EnergyStation()
                {
                    Energy = 100,
                    RecoveryRate = 30,
                    Position = new Position(10, 11)
                },
                new EnergyStation()
                {
                    Energy = 100,
                    RecoveryRate = 30,
                    Position = new Position(10, 10)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(15, 15)
                }
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot()
                {
                    Energy = 400,
                    Position = new Position(15, 10),
                    OwnerName = "Vantukh Matvii",
                }
            };
            Map map = new Map()
            {
                Stations = energyStations,
            };
            var step1 = algorythm.DoStep(robots, 0, map);
            robots[0].Position = ((MoveCommand)step1).NewPosition;
            var step2 = algorythm.DoStep(robots, 0, map);
            robots.Add(new Robot.Common.Robot()
            {
                Energy = ((CreateNewRobotCommand)step2).NewRobotEnergy,
                Position = new Position(robots[0].Position.X, robots[0].Position.Y + 1),
                OwnerName = "Vantukh Matvii",
            });
            robots[0].Energy = 100;
            var step3 = algorythm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(15,13), ((MoveCommand)step3).NewPosition);
        }

        [TestMethod]
        public void TestIfRobotWithBiggerIndexGoesToTheFreeCluster()
        {
            var algorythm = new VantukhAlgorythm();
            List<EnergyStation> energyStations = new List<EnergyStation>()
            {
                new EnergyStation()
                {
                    Energy = 100,
                    RecoveryRate = 30,
                    Position = new Position(10, 11)
                },
                new EnergyStation()
                {
                    Energy = 100,
                    RecoveryRate = 30,
                    Position = new Position(10, 10)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(15, 16)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(16, 15)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(15, 15)
                },
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot()
                {
                    Energy = 400,
                    Position = new Position(16, 16),
                    OwnerName = "Bolinovskii Andrii",
                },
                new Robot.Common.Robot()
                {
                    Energy = 400,
                    Position = new Position(15, 10),
                    OwnerName = "Vantukh Matvii",
                },
                
            };
            Map map = new Map()
            {
                Stations = energyStations,
            };
            var step1 = algorythm.DoStep(robots, 1, map);
            Assert.AreEqual(new Position(10, 10), ((MoveCommand)step1).NewPosition);
        }

        [TestMethod]
        public void TestIfRobotWithLowerIndexGoesToTheCluster()
        {
            var algorythm = new VantukhAlgorythm();
            List<EnergyStation> energyStations = new List<EnergyStation>()
            {
                new EnergyStation()
                {
                    Energy = 100,
                    RecoveryRate = 30,
                    Position = new Position(10, 11)
                },
                new EnergyStation()
                {
                    Energy = 100,
                    RecoveryRate = 30,
                    Position = new Position(10, 10)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(15, 16)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(16, 15)
                },
                new EnergyStation()
                {
                    Energy = 1000,
                    RecoveryRate = 30,
                    Position = new Position(15, 15)
                },
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>() {
                new Robot.Common.Robot()
                {
                    Energy = 400,
                    Position = new Position(15, 10),
                    OwnerName = "Vantukh Matvii",
                },
                new Robot.Common.Robot()
                {
                    Energy = 400,
                    Position = new Position(16, 16),
                    OwnerName = "Bolinovskii Andrii",
                },
                

            };
            Map map = new Map()
            {
                Stations = energyStations,
            };
            var step1 = algorythm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(15, 15), ((MoveCommand)step1).NewPosition);
        }

        [TestMethod]
        public void TestDistanceCalculations()
        {
            Position a = new Position(0, 0);
            Position b = new Position(10, 10);
            double distance = Math.Round(CalculationsHelper.DistanceBetweenPositions(a, b));
            Assert.AreEqual(14.0, distance);
        }

        [TestMethod]
        public void TestEnergyCalculations()
        {
            Position a = new Position(0, 0);
            Position b = new Position(10, 10);
            double distance = Math.Round(CalculationsHelper.EnergyPriceBetweenPositions(a, b));
            Assert.AreEqual(200, distance);
        }

        [TestMethod]
        public void TestEnergyCalculationsPerTurns()
        {
            Position a = new Position(0, 0);
            Position b = new Position(10, 10);
            double distance = Math.Round(CalculationsHelper.EnergyPriceBetweenPositions(a, b, 5));
            Assert.AreEqual(40, distance);
        }
    }
}
