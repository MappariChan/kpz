﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Vantukh.Matvii.RobotChallenge
{
    public class CalculationsHelper
    {
        public static double EnergyPriceBetweenPositions(Position a, Position b)
        {
            return Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2);
        }

        public static double EnergyPriceBetweenPositions(Position a, Position b, int turnAmount)
        {
            return EnergyPriceBetweenPositions(a, b) / turnAmount;
        }

        public static double DistanceBetweenPositions(Position a, Position b)
        {
            return Math.Sqrt(EnergyPriceBetweenPositions(a, b));
        }

        public static Queue<Position> AllPositionsPerTurnAmount(Position a, Position b, int turnAmount)
        {
            Queue<Position> positions = new Queue<Position>();
            for (int i = 1; i <= turnAmount; i++)
            {
                int x = a.X + (int)Math.Round(((double)(b.X - a.X)) / turnAmount * i);
                int y = a.Y + (int)Math.Round(((double)(b.Y - a.Y)) / turnAmount * i);
                positions.Enqueue(new Position(x, y));
            }
            return positions;
        }

        public static int OptimalStepAmount(Robot.Common.Robot robot, Position position, IList<EnergyStation> stations, int stationColectionRadius)
        {
            int totalStationsEnergy = 0;
            int totalRecoveryRate = 0;
            for (int j = 0; j < stations.Count; j++)
            {
                if (Math.Abs(stations[j].Position.X - position.X) <= stationColectionRadius &&
                    Math.Abs(stations[j].Position.Y - position.Y) <= stationColectionRadius)
                {
                    totalStationsEnergy += stations[j].Energy;
                    totalRecoveryRate += stations[j].RecoveryRate;
                }
            }
            int distance = (int)Math.Floor(DistanceBetweenPositions(robot.Position, position));
            for (int i = 1; i <= distance; i++)
            {
                int energyForArriving = robot.Energy - (int)Math.Floor(EnergyPriceBetweenPositions(robot.Position, position, i));
                if (energyForArriving < 0)
                {
                    continue;
                }
                int energyAfterArriving = energyForArriving + totalStationsEnergy + totalRecoveryRate * i;
                if (energyAfterArriving >= Constants.TheLowestEnergyForCreatingRobot)
                {
                    return i;
                }
            }
            return distance;
        }
    }
}
