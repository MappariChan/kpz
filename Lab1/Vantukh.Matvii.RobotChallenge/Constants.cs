﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vantukh.Matvii.RobotChallenge
{
    public class Constants
    {
        public static readonly int TheLowestEnergyForCreatingRobot = 300;
        public static readonly int AvgEnergyAmountGeneratedPerTurn = 30;
        public static readonly int MaxEnergyCollectonAmountPerTurn = 300;
    }
}
