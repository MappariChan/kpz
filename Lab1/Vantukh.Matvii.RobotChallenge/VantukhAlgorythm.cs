﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Vantukh.Matvii.RobotChallenge
{
    public class VantukhAlgorythm : IRobotAlgorithm
    {

        private List<RobotStepSchedule> robotStepSchedules;
        private List<StationCluster> stationClusters;
        private bool isItFirstStep;
        private int robotAmount = 10;
        private int roundNumber;
        private int firstRobotIndex;

        public string Author
        {
            get { return "Vantukh Matvii"; }
        }

        public VantukhAlgorythm()
        {
            robotStepSchedules = new List<RobotStepSchedule>();
            stationClusters = new List<StationCluster>();
            isItFirstStep = true;
        }

        private bool CheckIfThereIsNoRobot(Position stationPosition, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot.Position == stationPosition)
                {
                    return false;
                }
            }
            return true;
        }

        private int GetLowestRobotIndexNearbyToPosition(Position position, IList<Robot.Common.Robot> robots, int stationCollectionRadius)
        {
            int lowestIndex = int.MaxValue;
            for (int i = 0; i < robots.Count; i++)
            {
                if (Math.Abs(robots[i].Position.X - position.X) <= stationCollectionRadius * 2 &&
                    Math.Abs(robots[i].Position.Y - position.Y) <= stationCollectionRadius * 2 &&
                    i < lowestIndex)
                {
                    lowestIndex = i;
                }
            }
            return lowestIndex;
        }

        private int GetLowestEnemyRobotIndexNearbyToPosition(Position position, IList<Robot.Common.Robot> robots, int stationCollectionRadius)
        {
            var enemyRobots = robots.Where(robot => !robot.OwnerName.Equals(Author)).ToList();
            return GetLowestRobotIndexNearbyToPosition(position, enemyRobots, stationCollectionRadius);
        }

        public Position FindBestStationClusterPosition(int currentRobotIndex, IList<Robot.Common.Robot> robots)
        {
            Position bestStationClusterPosition = stationClusters.
                Where(cluster => GetLowestRobotIndexNearbyToPosition(cluster.ClusterPosition, robots, 2) > currentRobotIndex &&
                robotStepSchedules.Count(stepSchedul => stepSchedul.LastPosition == cluster.ClusterPosition) == 0 &&
                CalculationsHelper.EnergyPriceBetweenPositions(cluster.ClusterPosition, robots[currentRobotIndex].Position, (int)Math.Ceiling(((double)roundNumber)/3)) <= 100).
                OrderByDescending(cluster => cluster.NearbyStationAmount).
                ThenBy(cluster => CalculationsHelper.DistanceBetweenPositions(robots[currentRobotIndex].Position, cluster.ClusterPosition)).
                First().ClusterPosition;

            return bestStationClusterPosition;
        }

        public void FindAllStationClusters(IList<EnergyStation> stations, int stationsCollectionRadius) 
        {
            for (int i = stationsCollectionRadius; i < 100-stationsCollectionRadius; i++) 
            {
                for (int j = stationsCollectionRadius; j < 100-stationsCollectionRadius; j++)
                {
                    int nearbyStationsAmount = 0;
                    foreach(var station in stations)
                    {
                        if (Math.Abs(station.Position.Y - i) <= stationsCollectionRadius && Math.Abs(station.Position.X - j) <= stationsCollectionRadius)
                        {
                            nearbyStationsAmount++;
                        }
                    }
                    stationClusters.Add(new StationCluster()
                    {
                        ClusterPosition = new Position(j, i),
                        NearbyStationAmount = nearbyStationsAmount,
                    });
                }
            }
        }

        private int TotalEnergyOfNearbyStations(Position position, IList<EnergyStation> stations, int stationsCollectionRadius)
        {
            int totalEnergy = stations.Where(station => Math.Abs(station.Position.X - position.X) <= stationsCollectionRadius &&
            Math.Abs(station.Position.Y - position.Y) <= stationsCollectionRadius).Sum(station => station.Energy);
            return totalEnergy;
        }

        private int TotalRecoveryRateOfNearbyStations(Position position, IList<EnergyStation> stations, int stationsCollectionRadius)
        {
            int totalRecoveryRate = stations.Where(station => Math.Abs(station.Position.X - position.X) <= stationsCollectionRadius &&
            Math.Abs(station.Position.Y - position.Y) <= stationsCollectionRadius).Sum(station => station.RecoveryRate);
            return totalRecoveryRate;
        }

        private int AnotherFriendlyRobotAmountInThisCluster(Robot.Common.Robot currentRobot, IList<Robot.Common.Robot> robots)
        {
            int amount = 0;
            foreach (var robot in robots)
            {
                if (robot.OwnerName.Equals(Author) && currentRobot.Position != robot.Position && Math.Abs(robot.Position.X - currentRobot.Position.X) <= 1 &&
                    Math.Abs(robot.Position.Y - currentRobot.Position.Y) <= 1)
                {
                    amount++;
                }
            }
            return amount;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot currentRobot = robots[robotToMoveIndex];
            RobotStepSchedule[] stepSchedule = robotStepSchedules.Where(el => el.RobotIndex == robotToMoveIndex).ToArray();
            if (isItFirstStep)
            {
                roundNumber = 1;
                firstRobotIndex = robotToMoveIndex;
                FindAllStationClusters(map.Stations, 2);
                isItFirstStep = false;
            }
            else if (robotToMoveIndex == firstRobotIndex)
            {
                roundNumber++;
            }
            if (stepSchedule.Length != 0 && stepSchedule[0].LastPosition == currentRobot.Position && GetLowestEnemyRobotIndexNearbyToPosition(stepSchedule[0].LastPosition, robots, 2) > robotToMoveIndex)
            {
                if (currentRobot.Energy >= Constants.TheLowestEnergyForCreatingRobot && robotAmount < 100)
                {
                    robotAmount += 1;
                    return new CreateNewRobotCommand() { NewRobotEnergy = 200 };
                }
                if (TotalEnergyOfNearbyStations(currentRobot.Position, map.Stations, 2) +
                    TotalRecoveryRateOfNearbyStations(currentRobot.Position, map.Stations, 2)*5 -
                    AnotherFriendlyRobotAmountInThisCluster(currentRobot, robots) *
                    Constants.MaxEnergyCollectonAmountPerTurn * 5 > 0)
                {
                    return new CollectEnergyCommand();
                }
                Position bestPosition = FindBestStationClusterPosition(robotToMoveIndex, robots);
                int optimalStepAmount = CalculationsHelper.OptimalStepAmount(currentRobot, bestPosition, map.Stations, 2);
                stepSchedule[0].StepSchedule =
                    CalculationsHelper.AllPositionsPerTurnAmount(currentRobot.Position, bestPosition, optimalStepAmount);
                stepSchedule[0].LastPosition = bestPosition;
                Position nextPos = stepSchedule[0].StepSchedule.Dequeue();
                while (!CheckIfThereIsNoRobot(nextPos, robots))
                {
                    nextPos = stepSchedule[0].StepSchedule.Dequeue();
                }
                return new MoveCommand() { NewPosition = nextPos };
            }
            if (stepSchedule.Length == 0)
            {
                Position bestPosition = FindBestStationClusterPosition(robotToMoveIndex, robots);
                int optimalStepAmount = CalculationsHelper.OptimalStepAmount(currentRobot, bestPosition, map.Stations, 2);
                robotStepSchedules.Add(new RobotStepSchedule(robotToMoveIndex,
                    CalculationsHelper.AllPositionsPerTurnAmount(currentRobot.Position, bestPosition, optimalStepAmount),
                    bestPosition));
                stepSchedule = robotStepSchedules.Where(el => el.RobotIndex == robotToMoveIndex).ToArray();
            }
            
            RobotStepSchedule currentRobotStepSchedule = stepSchedule[0];
            if (GetLowestRobotIndexNearbyToPosition(currentRobotStepSchedule.LastPosition, robots, 2) < robotToMoveIndex)
            {
                Position bestPosition = FindBestStationClusterPosition(robotToMoveIndex, robots);
                int optimalStepAmount = CalculationsHelper.OptimalStepAmount(currentRobot, bestPosition, map.Stations, 2);
                currentRobotStepSchedule.StepSchedule =
                    CalculationsHelper.AllPositionsPerTurnAmount(currentRobot.Position, bestPosition, optimalStepAmount);
                currentRobotStepSchedule.LastPosition = bestPosition;
            }
            Position nextPosition = currentRobotStepSchedule.StepSchedule.Dequeue();
            while (!CheckIfThereIsNoRobot(nextPosition, robots))
            {
                nextPosition = currentRobotStepSchedule.StepSchedule.Dequeue();
            }
            return new MoveCommand() { NewPosition = nextPosition };
        }
        private class RobotStepSchedule
        {
            public int RobotIndex { get; set; }
            public Queue<Position> StepSchedule { get; set; }
            public Position LastPosition { get; set; }

            public RobotStepSchedule()
            {
                StepSchedule = new Queue<Position>();
            }

            public RobotStepSchedule(int robotIndex, Queue<Position> stepSchedule, Position lastPosition)
            {
                RobotIndex = robotIndex;
                StepSchedule = stepSchedule;
                LastPosition = lastPosition;
            }
        }

        private class StationCluster
        { 
            public Position ClusterPosition { get; set; }
            public int NearbyStationAmount { get; set; }
        }
    }
}
