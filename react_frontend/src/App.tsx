import React from "react";
import {
  LoaderFunction,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import MoviePage from "./pages/MoviePage";
import { Provider } from "react-redux";
import store from "./store/store";
import RootLayout from "./layouts/RootLayout";
import { routeGuard } from "./utils/auth";

import "./App.module.css";
import HallPage from "./pages/HallPage";
import Unauthorized from "./pages/Unauthorized";

const browserRouter: any = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    children: [
      {
        path: "",
        element: <MoviePage />,
        loader: routeGuard,
      },
      { path: "halls", element: <HallPage /> },
      { path: "unauthorized", element: <Unauthorized /> },
    ],
  },
]);

function App() {
  return (
    <Provider store={store}>
      <RouterProvider router={browserRouter} />
    </Provider>
  );
}

export default App;
