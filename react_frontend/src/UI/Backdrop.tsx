import { MouseEventHandler } from "react";

import classes from "./Backdrop.module.css";

const Backdrop = (props: { onClose: any }) => {
  return <div className={classes.backdrop} onClick={props.onClose} />;
};

export default Backdrop;
