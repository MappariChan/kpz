import classes from "./Button.module.css";

const Button = (props: {
  title: string;
  onClick?: any;
  type: "submit" | "reset" | "button" | undefined;
}) => {
  return (
    <button
      className={classes.button}
      type={props.type}
      onClick={props.onClick}
    >
      {props.title}
    </button>
  );
};

export default Button;
