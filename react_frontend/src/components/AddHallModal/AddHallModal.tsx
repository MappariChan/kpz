import { useState } from "react";
import { useAppDispatch } from "../../store/store";
import { addHallAction } from "../../store/slices/hallSlice";
import Backdrop from "../../UI/Backdrop";

import classes from "./AddHallModal.module.css";
import Button from "../../UI/Button";

const AddHallModal = (props: { onClose: any }) => {
  const [name, setName] = useState("");
  const dispatch = useAppDispatch();

  const submitHandler = () => {
    dispatch(addHallAction({ name }));
    props.onClose();
  };

  return (
    <>
      <form
        className={classes.form}
        onSubmit={(e) => {
          e.preventDefault();
          submitHandler();
        }}
      >
        <span>Name</span>
        <input value={name} onChange={(e) => setName(e.target.value)} />
        <div className={classes.actions}>
          <Button title="Add" type="submit" />
          <Button onClick={props.onClose} title="Cancel" type="button" />
        </div>
      </form>
      <Backdrop onClose={props.onClose} />
    </>
  );
};

export default AddHallModal;
