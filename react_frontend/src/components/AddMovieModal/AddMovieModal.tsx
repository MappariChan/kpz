import { useState } from "react";
import { useAppDispatch } from "../../store/store";
import { addMovieAction } from "../../store/slices/movieSlice";
import Backdrop from "../../UI/Backdrop";

import classes from "./AddMovieModal.module.css";
import Button from "../../UI/Button";

const AddMovieModal = (props: { onClose: any }) => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const dispatch = useAppDispatch();

  const submitHandler = () => {
    dispatch(addMovieAction({ name, description }));
    props.onClose();
  };

  return (
    <>
      <form
        className={classes.form}
        onSubmit={(e) => {
          e.preventDefault();
          submitHandler();
        }}
      >
        <span>Name</span>
        <input value={name} onChange={(e) => setName(e.target.value)} />
        <span>Description</span>
        <textarea
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <div className={classes.actions}>
          <Button title="Add" type="submit" />
          <Button onClick={props.onClose} title="Cancel" type="button" />
        </div>
      </form>
      <Backdrop onClose={props.onClose} />
    </>
  );
};

export default AddMovieModal;
