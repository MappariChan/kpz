import { Hall } from "../../models/Hall";
import {
  deleteHallAction,
  updateHallAction,
} from "../../store/slices/hallSlice";
import { useAppDispatch } from "../../store/store";
import { useState } from "react";
import { createPortal } from "react-dom";

import classes from "./HallTable.module.css";
import Button from "../../UI/Button";
import AddHallModal from "../AddHallModal/AddHallModal";

const HallTableItem = (props: { hall: Hall }) => {
  const dispatch = useAppDispatch();
  const [isEditEnabled, setEditEnabled] = useState(false);
  const [name, setName] = useState(props.hall.name);

  const deleteHandler = () => {
    dispatch(deleteHallAction(props.hall.id));
  };

  const saveHandler = () => {
    dispatch(updateHallAction(props.hall.id, { name }));
    setEditEnabled(false);
  };

  return (
    <tr>
      <td>
        <textarea
          disabled={!isEditEnabled}
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </td>
      <td className={classes.actions}>
        <Button type="button" onClick={deleteHandler} title="delete" />
        {isEditEnabled ? (
          <Button type="button" onClick={saveHandler} title="save" />
        ) : (
          <Button
            type="button"
            onClick={() => setEditEnabled(true)}
            title="edit"
          />
        )}
      </td>
      <td></td>
    </tr>
  );
};

const HallTable = (props: { halls: Hall[] }) => {
  const [isModalShown, setModalShown] = useState(false);
  const modalContainer = document.querySelector("#modal");
  return (
    <>
      {isModalShown && modalContainer != null ? (
        createPortal(
          <AddHallModal onClose={() => setModalShown(false)} />,
          modalContainer
        )
      ) : (
        <></>
      )}
      <table className={classes.table}>
        <thead>
          <tr>
            <th>Name</th>
            <th>Actions</th>
            <th>
              <Button
                title="+"
                onClick={() => setModalShown(true)}
                type="button"
              />
            </th>
          </tr>
        </thead>
        <tbody>
          {props.halls.map((hall) => (
            <HallTableItem key={hall.id} hall={hall} />
          ))}
        </tbody>
      </table>
    </>
  );
};

export default HallTable;
