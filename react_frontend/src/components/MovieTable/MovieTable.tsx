import { Movie } from "../../models/Movie";
import {
  deleteMovieAction,
  updateMovieAction,
} from "../../store/slices/movieSlice";
import { useAppDispatch } from "../../store/store";
import { useState } from "react";
import { createPortal } from "react-dom";

import classes from "./MovieTable.module.css";
import Button from "../../UI/Button";
import AddMovieModal from "../AddMovieModal/AddMovieModal";

function capitalizeFirstLetter(string: string): string {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const MovieTableItem = (props: { movie: Movie }) => {
  const dispatch = useAppDispatch();
  const [isEditEnabled, setEditEnabled] = useState(false);
  const [name, setName] = useState(props.movie.name);
  const [description, setDescription] = useState(props.movie.description);

  const deleteHandler = () => {
    dispatch(deleteMovieAction(props.movie.id));
  };

  const saveHandler = () => {
    dispatch(updateMovieAction(props.movie.id, { name, description }));
    setEditEnabled(false);
  };

  return (
    <tr>
      <td>
        <textarea
          disabled={!isEditEnabled}
          value={capitalizeFirstLetter(name)}
          onChange={(e) => setName(e.target.value)}
        />
      </td>
      <td>
        <textarea
          disabled={!isEditEnabled}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </td>
      <td className={classes.actions}>
        <Button type="button" onClick={deleteHandler} title="delete" />
        {isEditEnabled ? (
          <Button type="button" onClick={saveHandler} title="save" />
        ) : (
          <Button
            type="button"
            onClick={() => setEditEnabled(true)}
            title="edit"
          />
        )}
      </td>
      <td></td>
    </tr>
  );
};

const MovieTable = (props: { movies: Movie[] }) => {
  const [isModalShown, setModalShown] = useState(false);
  const modalContainer = document.querySelector("#modal");
  return (
    <>
      {isModalShown && modalContainer != null ? (
        createPortal(
          <AddMovieModal onClose={() => setModalShown(false)} />,
          modalContainer
        )
      ) : (
        <></>
      )}
      <table className={classes.table}>
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Actions</th>
            <th>
              <Button
                title="+"
                onClick={() => setModalShown(true)}
                type="button"
              />
            </th>
          </tr>
        </thead>
        <tbody>
          {props.movies.map((movie) => (
            <MovieTableItem key={movie.id} movie={movie} />
          ))}
        </tbody>
      </table>
    </>
  );
};

export default MovieTable;
