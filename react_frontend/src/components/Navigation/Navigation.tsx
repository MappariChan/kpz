import { Link } from "react-router-dom";

import classes from "./Navigation.module.css";

const Navigation = () => {
  return (
    <head className={classes.head}>
      <h1>Cinema</h1>
      <nav>
        <ul>
          <li>
            <Link to="/">Movies</Link>
          </li>
          <li>
            <Link to="/halls">Halls</Link>
          </li>
        </ul>
      </nav>
    </head>
  );
};

export default Navigation;
