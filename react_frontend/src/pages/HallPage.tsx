import { useSelector } from "react-redux";
import { AppDispatch, useAppDispatch, RootState } from "../store/store";
import { useEffect } from "react";

import { Hall } from "../models/Hall";
import HallTable from "../components/HallTable/HallTable";
import { getHallsAction } from "../store/slices/hallSlice";

const HallPage = () => {
  const halls: Hall[] = useSelector(
    (state: RootState) => state.hallSlice.halls
  );
  const dispatch: AppDispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getHallsAction());
  }, []);

  const modalContainer = document.querySelector("#modal");

  return <HallTable halls={halls} />;
};

export default HallPage;
