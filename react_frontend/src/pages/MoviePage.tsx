import { useSelector } from "react-redux";
import { AppDispatch, useAppDispatch, RootState } from "../store/store";
import { useEffect } from "react";

import { Movie } from "../models/Movie";
import MovieTable from "../components/MovieTable/MovieTable";
import { getMoviesAction } from "../store/slices/movieSlice";

const MoviePage = () => {
  const movies: Movie[] = useSelector(
    (state: RootState) => state.movieSlice.movies
  );
  const dispatch: AppDispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getMoviesAction());
  }, []);

  const modalContainer = document.querySelector("#modal");

  return <MovieTable movies={movies} />;
};

export default MoviePage;
