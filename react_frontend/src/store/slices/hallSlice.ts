import { createSlice } from "@reduxjs/toolkit";
import { Hall } from "../../models/Hall";
import { AppDispatch } from "../store";

const hallInitialState: {
  halls: Hall[];
} = { halls: [] };

const hallSlice = createSlice({
  name: "hallSlice",
  initialState: hallInitialState,
  reducers: {
    getHalls: (state, action) => {
      state.halls = action.payload;
    },
    addHall: (state, action) => {
      state.halls.unshift(action.payload);
    },
    removeHall: (state, action) => {
      state.halls = state.halls.filter((m) => m.id != action.payload.hallId);
    },
    updateHall: (state, action) => {
      const hallToUpdate = state.halls.filter(
        (m) => m.id === action.payload.hallId
      )[0];
      hallToUpdate.name = action.payload.hall.name;
    },
  },
});

export const getHallsAction = () => {
  return async (dispatch: AppDispatch) => {
    const response = await fetch("https://localhost:7037/api/hall", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      const responseData = await response.json();
      dispatch(hallSlice.actions.getHalls(responseData.result));
    } else {
      alert("server error");
    }
  };
};

export const addHallAction = (hall: Hall) => {
  return async (dispatch: AppDispatch) => {
    const response = await fetch("https://localhost:7037/api/hall", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(hall),
    });
    if (response.ok) {
      const responseData = await response.json();
      dispatch(hallSlice.actions.addHall(responseData.result));
    } else {
      alert("server error");
    }
  };
};

export const deleteHallAction = (hallId: number | undefined) => {
  return async (dispatch: AppDispatch) => {
    const response = await fetch(`https://localhost:7037/api/hall/${hallId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      const responseData = await response.json();
      console.log(responseData.result);
      dispatch(hallSlice.actions.removeHall({ hallId }));
    } else {
      alert("server error");
    }
  };
};

export const updateHallAction = (hallId: number | undefined, hall: Hall) => {
  return async (dispatch: AppDispatch) => {
    const response = await fetch(`https://localhost:7037/api/hall/${hallId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(hall),
    });
    if (response.ok) {
      const responseData = await response.json();
      console.log(responseData.result);
      dispatch(hallSlice.actions.updateHall({ hallId, hall }));
    } else {
      alert("server error");
    }
  };
};

export default hallSlice;
