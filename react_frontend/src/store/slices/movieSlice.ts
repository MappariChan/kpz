import { createSlice } from "@reduxjs/toolkit";
import { Movie } from "../../models/Movie";
import { AppDispatch } from "../store";

const movieInitialState: {
  movies: Movie[];
} = { movies: [] };

const movieSlice = createSlice({
  name: "movieSlice",
  initialState: movieInitialState,
  reducers: {
    getMovies: (state, action) => {
      state.movies = action.payload;
    },
    addMovie: (state, action) => {
      state.movies.unshift(action.payload);
    },
    removeMovie: (state, action) => {
      state.movies = state.movies.filter((m) => m.id != action.payload.movieId);
    },
    updateMovie: (state, action) => {
      const movieToUpdate = state.movies.filter(
        (m) => m.id === action.payload.movieId
      )[0];
      movieToUpdate.name = action.payload.movie.name;
      movieToUpdate.description = action.payload.movie.description;
    },
  },
});

export const getMoviesAction = () => {
  return async (dispatch: AppDispatch) => {
    const response = await fetch("https://localhost:7037/api/movie", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      const responseData = await response.json();
      dispatch(movieSlice.actions.getMovies(responseData.result));
    } else {
      alert("server error");
    }
  };
};

export const addMovieAction = (movie: Movie) => {
  return async (dispatch: AppDispatch) => {
    const response = await fetch("https://localhost:7037/api/movie", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(movie),
    });
    if (response.ok) {
      dispatch(movieSlice.actions.addMovie({ ...movie, id: movie.name }));
    } else {
      const responseData = await response.json();
      alert(responseData.errors);
    }
  };
};

export const deleteMovieAction = (movieId: number | undefined) => {
  return async (dispatch: AppDispatch) => {
    const response = await fetch(
      `https://localhost:7037/api/movie/${movieId}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.ok) {
      const responseData = await response.json();
      console.log(responseData.result);
      dispatch(movieSlice.actions.removeMovie({ movieId }));
    } else {
      alert("server error");
    }
  };
};

export const updateMovieAction = (
  movieId: number | undefined,
  movie: Movie
) => {
  return async (dispatch: AppDispatch) => {
    const response = await fetch(
      `https://localhost:7037/api/movie/${movieId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(movie),
      }
    );
    if (response.ok) {
      const responseData = await response.json();
      console.log(responseData.result);
      dispatch(movieSlice.actions.updateMovie({ movieId, movie }));
    } else {
      alert("server error");
    }
  };
};

export default movieSlice;
