import { configureStore } from "@reduxjs/toolkit";

import movieSlice from "./slices/movieSlice";
import { useDispatch } from "react-redux";
import hallSlice from "./slices/hallSlice";

const store = configureStore({
  reducer: {
    movieSlice: movieSlice.reducer,
    hallSlice: hallSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();

export default store;
